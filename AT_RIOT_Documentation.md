# AT

## The end of line charactere is manage by the module at

In file at.h :

    /**
    * @brief End of line character to send after the AT command.
    */
    #if IS_ACTIVE(CONFIG_AT_SEND_EOL_WINDOWS)
    #define CONFIG_AT_SEND_EOL   "\r\n"
    #elif IS_ACTIVE(CONFIG_AT_SEND_EOL_UNIX)
    #define CONFIG_AT_SEND_EOL   "\n"
    #elif IS_ACTIVE(CONFIG_AT_SEND_EOL_MAC)
    #define CONFIG_AT_SEND_EOL   "\r"
    #endif

    #ifndef CONFIG_AT_SEND_EOL
    #define CONFIG_AT_SEND_EOL "\r"
    #endif

when calling *at_send_command* define in file at.c : 

    int at_send_cmd(at_dev_t *dev, const char *command, uint32_t timeout)
    {
        size_t cmdlen = strlen(command);

        uart_write(dev->uart, (const uint8_t *)command, cmdlen);
        uart_write(dev->uart, (const uint8_t *)CONFIG_AT_SEND_EOL, AT_SEND_EOL_LEN);

        if (AT_SEND_ECHO) {
            if (at_expect_bytes(dev, command, timeout)) {
                return -1;
            }

            if (at_expect_bytes(dev, CONFIG_AT_SEND_EOL AT_RECV_EOL_1 AT_RECV_EOL_2, timeout)) {
                return -2;
            }
        }

        return 0;
    }

