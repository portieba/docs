# **Follow up sheet**

## **Team**
**Tutor :** Didier Donsez <br>
**Members :** Eric Herque - Barnabe Portier - Guillaume Mallen <br/>
**Departement :** INFO4 - Polytech Grenoble <br/>

## **Week 1 : 18/01**
* Project attribution
* We gave the different informations of the group asked

GM : communicates with Mr. Didier Donsez by email for the different informations asked

## **Week 2 : 25/01**
* Following the course about Lora and RIOT by Didier DONSEZ
* Recovery of 2 LRWAN2 kits

GM : I assisted to the course (Monday : 180 minutes). I picked up the LRWAN2 kits from Roman's group and dispatched them to my group. (Wednesday)

EH: assisted to the course on 25/01 (180 minutes)

## **Week 3 : 01/02**
* Weekly meeting with our tutor for project monitoring
* Splitting the project into different tasks : 
    * writing a device driver on RIOT for both INucleo 868 and Rising HF 443 modems -> AT commands
    * writing a device driver on RIOT for the concentrator board Nucleo F722ZE
* We followed the [P-NUCLEO-LRWAN Starter Packs](https://github.com/CampusIoT/tutorial/tree/master/p-nucleo-lrwan) and 
[Tutoriel LoRaWAN](https://docs.google.com/document/d/1x3DzT-MLrBhOk9UZf93lYh7wYgSPrwt45Ysy_ajh7Gg/edit) tutorials
* distribution of tasks within the group : 
    * Guillaume : kit LRWAN3
    * Eric : kit LRWAN2
    * Barnabé : kit LRWAN2

GM : I followed the tutorial [P-NUCLEO-LRWAN Starter Packs](https://github.com/CampusIoT/tutorial/tree/master/p-nucleo-lrwan). I got blocked because my computer did not recognize the board (Monday : 60 minutes). Eric helped me without success and shown me the result obtained with his board. (Monday : 120 minutes)

EH: followed the [P-NUCLEO-LRWAN Starter Packs](https://github.com/CampusIoT/tutorial/tree/master/p-nucleo-lrwan) tutorial on 31/01 (180 minutes):
	* documentation about how to use minicom: 60 minutes
	* following the tutorial + problems: 120 minutes


## **Week 4 : 08/02**
* Weekly meeting with our tutor for project monitoring
* We downloaded the STM32cubeIDE IDE
* We followed the tutorial [RIOT](https://www.fun-mooc.fr/courses/course-v1:inria+41020+session02/info) : learned about IoT's functionnement and goals 

GM : Eric and me met together at polytech to solve my problem with my board (Wednesday 10/02, 30 minutes). I followed the beginning of the course on [RIOT](https://www.fun-mooc.fr/courses/course-v1:inria+41020+session02/info) : Module 1 (Monday 08/02 : 150 minutes).

EH: helped Guillaume with his board connection problems on 10/02 (30 minutes). Finished the two first modules from INRIA's fun-mooc from 11/02 to 18/02 (300 minutes).

## **Holidays**
GM : Tried to modify the LRWAN_NS1 kit programm using STM32cubeIDE (~90 minutes)

## **Week 5 : 22/02**
* Weekly meeting with our tutor for project monitoring
* End of the [RIOT](https://www.fun-mooc.fr/courses/course-v1:inria+41020+session02/info) tutorial : learned about firmwares and RIOT (First RIOT application, managing thread with RIOT, use timers, use sensors on a board )

GM : I finished the [RIOT](https://www.fun-mooc.fr/courses/course-v1:inria+41020+session02/info) tutorial : Module 2 (Monday 22/02 : 120 minutes) and Module 3 (tuesday 08/02 : 180 minutes)

EH: finished the third module from INRIA’s fun-mooc from 22/02 to 23/02, (~180 minutes)



## **Week 6 : 01/03**
* Weekly meeting with our tutor for project monitoring
* made our first board flash : a simple hello world RIOT OS application flashed on our nucleo-l073rz board
* trying to send an AT command without success
* Preparation of the mid-term presentation

GM : Started to pull and review RIOT's code, tried to compile example codes in Native mode with Eric (Monday 01/03 : 180 minutes) </br>
Had a gcc-arm-compiler problem, I fixed it (30 minutes) </br>
With Eric we flashed our boards for the first time, tried different RIOT codes, implemented and run our first « hello world » code, started to write AT-commands code
(02/03, 240 minutes) </br>
Tried to understand why our code bugs and how to fix it (04/03 : 120 minutes) </br>
Research on AT RIOT library and how to use it (05/03 : 120 minutes) </br>
Group meeting to prepare the mid-term presentation (Sunday 07/03 : 60 minutes)

EH: Started to pull and review RIOT’s code, tried to compile example codes in Native mode without success, with Guillaume + set up git repository and forked RIOT-OS repository + install openocd: 01/03, 180 minutes </br>
Encountered and solved a problem with STM32cubeIDE IDE’s install + tried to help Guillaume with his gcc-arm compiler problem: 01/03, 60 minutes (+ 60 minutes from above) </br>
With Guillaume we flashed our boards for the first time, tried different RIOT codes, implemented and run our first « hello world » program, then started to write AT-commands code: 02/03, 240 minutes </br>
Tried to understand why our program didn’t work, where the AT-commands code bugged and how to fix it : 04/03, 120 minutes </br>
Documentation + researches about at_dev_init function and UART_DEV and how to implement these with RIOT: 05/03, 120 minutes </br>
Preparation for the mid-term speech with the whole team: 07/03, 60 minutes </br>


## **Week 7 : 08/03**
* We made our mid-term presentation 
* We kept trying to code a program to send an AT command
* We focused more on boards documentation and on UART aspect
* We made the documentation for the different tutorials we followed
* As asked, we improve our follow up sheet
* We made a clone of RIOT and created 2 branches : wip/lrwan2/endpoint and wip/lrwan3/endpoint

GM : I tried again without success to send an AT command. I made research on UART to understand more how to initialize our UART in our RIOT programm. Sent an email to our tutor in order to be unlocked. (tuesday : 180 minutes). </br>
As asked, with Eric we improve the follow up sheet (10/03 : 100 minutes). </br>
Documentation (Saturday 13/02 : 180 minutes).


EH: Researches about UART: 09/03, 60 minutes </br>
Improvement of the follow up sheet : </br>
10/03, 100 minutes </br>
Documentation about how to stard P-NUCLEO-LRWAN kits: 90 minutes