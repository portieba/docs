# RIOT

## Introduction

### Programming a connected object
A firmware is a program for a microcontroller wich contains the application code,the bootloader code and the code needed to interface with the hardware, the kernel and for the librairies used.

The source code is writting in a high level language and then compiled in machine language. To send it to the microcontroller, we flashed it by a programmer (here we use OpenOCD wich is used for the ARM architecture).

### What is an Operating System

the main characteristics of operating systems :
* Memory management 
* Hardware support
* Network connectivity
* Efficient power use
* Real-time capabilities
* Security


### OS used in IoT
Riot. 
Main characteristics :
* Real-Time Operating System
* Network : Bluetooth LE, WPAN, LPWAN, Wi-Fi/Ethernet
* Minimum memory footprint : ~1.5kb RAM, ~5kb ROM

## Discovering RIOT

### RIOT characteristics
    
System characteristics : It is real-time and requires very little memory space (in its most basic configuration, 2.8kB of RAM and 3.2kB of ROM). It provides an original mechanism for managing the sleep state of microcontrollers, enabling them to minimize their power consumption.

RIOT OS is based around a micro-kernel architecture. This contains only the building blocks needed in order for the system to function 
* A multi-task system (thread)
* A real-time scheduler
* A mechanism for communication between tasks (permits to deal with external interrups, without any risk of loosing an intermediary state.)
* Synchronization mechanisms (dealing with concurrency between thread)

The structure of a RIOT application -> In its most simple version, 2 Threads
* The main thread (where the main function is executed)
* The idle thread (with the lowest priority. Responsible for managing power consumption modes)

### The hardware abstraction layer

A hardware abstraction layer is added to this real-time micro-kernel, enabling RIOT to be execute on a wide range of hardware targets.

This hardware abstraction layer is divided into 4 levels :
* The CPU level, where certain element common to all platforms are defined
* The board level, where certain varaibles or macros common to all platforms are defined
* The hardware level, where generic APIs for the main types of hardware available to microcontrollers are defined
* The driver level, where all radio drivers, sensor drivers and actuator drivers can be found
    
When compiling an application you must specify the target for which the firmware is to be produced.

### A modular system

RIOT is based on a micro-kernel architecture, to which developers add the modules needed for their application. In order to limit the microcontroller's memory requirement (in kB), you only need ton include  what is actually necessary for an application.

RIOT's system provides a whole ecosystem of modules which can be split into multiples categories :
* System libraries (shell modules, xtimer module)
* Actuator and sensor drivers
* Network protocol
* External packages

### Network protocol stacks

RIOT support the main network protocols currently used for the IoT through what are know as network protocol stacks.

Some of these network stack are grouped together in a set designed for IP-oriented communication, making it possible for object to communicate with orher items of equipment over the internet.

### Source code structure

The source code is organized as follows: 
* boards, contains the code specific to the support of the boards
* core, contains the code relating to the kernel, to thread management and to inter-process communication
* cpu, contains the code specific to the support for the microcontrollers
* dist, contains the code for RIOT helper tools
* doc, contains Doxygen static documentation files
* drivers, contains the high level drivers for external modules
* examples
* makefiles
* pkg, contains all external packages adapted to RIOT
* sys, contains the system libraries code
* tests

### The build system

Goal : produce from an application's source code, the firmware that will be written on the microcontroller's flash memory. This build system also contains the rules for launching the write procedure on the flash memory, for opening a serial terminal to read the application's standard output...

How ? Thank's to make giving it as a parameter the makefile located in the directory for this application

Here is an example of application Makefile :

    # Give a name to the application
    APPLICATION = example_application

    # If not already specified, use native as default target board
    BOARD ?= native

    # List the modules, features, packages needed by this application.
    USEMODULE += xtimer

    # Specify the path the RIOT code base
    RIOTBASE ?= $(CURDIR)/../../RIOT

    # Final step: include the Makefile.include file, which contains the build
    # system logic and definitions of make targets
    include $(RIOTBASE)/Makefile.include

To compile, flash and connect to the board's serial port you can use just one command:
    
    cd <application dir>
    make BOARD=<your Board> flash term



    



    