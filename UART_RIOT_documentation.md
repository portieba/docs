# UART

*The simple interface provides capabilities to initialize and configure the serial communication module, which automatically enables for receiving data, as well as writing data to the UART port, which means transmitting data. The UART device and the corresponding pins need to be mapped in RIOT/boards/ * /include/periph_conf.h. Furthermore, you need to select the baudrate for initialization which is typically {9600, 19200, 38400, 57600, 115200} baud. Additionally, you should register a callback function that is executed in interrupt context when data is being received*

By default the UART_DEV(0) device of each board is initialized and mapped to STDIO in RIOT wich is used for standard input/output functions like **printf** or **puts()**.

