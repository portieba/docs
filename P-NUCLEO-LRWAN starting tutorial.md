# P-NUCLEO-LRWAN starting tutorials
## Introduction
In this document, we will focus on starting the gateways from the P-NUCLEO-LRWAN2 and P-NUCLEO-LRWAN3
packs.

**The P-NUCLEO-LRWAN2 contains:**
* LoRaWAN Gateway based on:
	* a Nucleo NUCLEO-F746ZG
	* a 868MHz LRWAN_GS_HF1 SX1301 from RisingHF
* LoRaWAN Endpoint based on:
	* a Nucleo NUCLEO-L073RZ 
	* a 868MHz USI I-NUCLEO-LRWAN1 ARDUINO
* An Antenna

**The P-NUCLEO-LRWAN3 contains:**
* LoRaWAN Gateway based on:
	* a Nucleo NUCLEO-F746ZG
	* a 433MHz LRWAN_GS_HF1 SX1301 from RisingHF
* LoRaWAN Endpoint based on:
	* a Nucleo NUCLEO-L073RZ 
	* a 433MHz RisingHF LRWAN_NS1 ARDUINO
* An Antenna

**Required gear: **
* Micro USB cable + 5V power supply
* Micro USB to USB cable

## Connection
How to connect the board to your machina in 3 steps:
1. Connect the antenna to the LRWAN_GS_HF1
2. Branch the 5V power supply on the LRWAN_GS_HF1 `CN1 port`
3. Connect the Nucleo's `CN1 port` to your machine's USB port
## LoRaWAN Gateway Configuration

LoRaWAN gateways can be reconfigured with AT commands. Prior to this, make sure that minicom is installed on your device, in order to do it.

Make sure to verify if the board is correctly connected by locating the port on your device (`/dev/tty.*` or `/dev/ttyACM0`).

### Starting the LRWAN2 Gateway

Launch Minicom: `minicom -s`

In the minicom menu, go on the "Serial Port Setup" section and configure:
* the USB port path
> Press on the "A" key, then write the USB port path that you saw earlier. In our example, it would be `/dev/tty*` or `/dev/ttyACM0`. 
* the baudrate
> Press the "E" key, then write the desired baudrate. In our case, it will be `115200 8N1`.

Once you are done with the settings, press Enter, then in the main menu, select "Save as dlf" before pressing "Exit".

After these steps you should have a terminal in which we can write down AT commands. We will now focus on our board configuration.

#### Network Server Configuration
First check what the current Network Server is with `AT+PKTFWD`. If you want to change it, enter informations in this format.
```
AT+PKTFWD
AT+PKTFWD=address,uplink_udp_port,downlink_udp_port
```

To make sure the configuration is effective, enter:
```
AT+RESET
```

#### NTP Server Configuration
First check what the NTP Server is.
```
AT+NTP
+NTP: "1.ubuntu.pool.ntp.org"
```
Then if needed, change the NTP Server configuration:
```
AT+NTP=0.europe.pool.ntp.org
+NTP: "0.EUROPE.POOL.NTP.ORG"
```
### Starting the LRWAN3 Gateway

Concerning the LRWAN3 Gateway it is quite similar, you have to follow the LRWAN2 configurations, and in addition to this you have to change the packet forwarder channels:
```
AT+CH
AT+CH=eu433 
```
> The board is, by default, configured on the ISM Chinese band. You have to change it in order to respect the european regulations.