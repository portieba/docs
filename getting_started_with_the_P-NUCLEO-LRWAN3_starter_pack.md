# Getting started with the P-NUCLEO-LRWAN3 starter pack

## LRWAN_NS1 sensor device setup and configuration

by default, the device is configured for the CN470Prequel frequency band and in the OTAA mode.

The LRWAN_NS1 LORA expansion board uses the serial interface (CN8 pin 1 (TX) and pin2 (RX)) for the AT command console. The default serial configuration is :
* Baud rate: 9600
* Data: 8 bit
* Parity: None
* Stop: 1 bit

### Terminal emulation software settings

* When receive a new line : CR (\r)
* When transmit a new line : CR+LF (\r\n)

### AT command list
* AT -> Return +OK
* HELP -> Prints help information
* FDEFAULT -> Resets to factory default settings
* RESET -> Software reset gateway
* SYS -> Checks all configuration
* VER -> Gets version
* LOG -> turns on/off packet forward log
* ECHO -> AT command echo on/off
* MAC -> Sets/gets the gateway MAC Adress
* IP -> DHCP/static IP control
* DNS -> Sets/gets the DNS adress
* NTP -> Sets/gets the NTP server adress
* EUI -> MAC adress (EUI48) to gateway ID (EUI64) padding
* LORAWAN -> LoRaWAN network selection (public/private)
* PKRFWD -> Packet forwarder server address and port settings
* CH -> Packet forwarder channels
* Baudrate -> AT command and logging UART interface baud rate