# Tests done

## A simple "Hello-World" RIOT application 

    #include <stdio.h>
    #include "xtimer.h"

    int main(void)
    {
        xtimer_sleep(5);
        puts("Hello from RIOT!");
        return 0;
    }


## Send an AT command

* Tried to compile the RIOT/tests/driver_at/main.c on our board
* Made our own mini test program 

        #include <stdio.h>
        #include <string.h>
        #include <stdlib.h>
        #include <periph/uart.h>

        #include "at.h"
        #include "shell.h"
        #include "xtimer.h"

        int main(void)
        {
            xtimer_sleep(5);
            puts("Hello from RIOT!");
            
        at_dev_t *dev = NULL;
        char *buf;
        buf = malloc(256*sizeof(char));

        int init = at_dev_init( dev, UART_DEV(1) , 9600, buf, 256);

        if(init == UART_OK) {
            puts("OK");
        }
        else{
            puts("NO OK");
        }

        at_send_cmd_wait_ok	(dev, "AT", 10 * US_PER_SEC);

        return 0;
        }

* Did not get the result expected for the moment